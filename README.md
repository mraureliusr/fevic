# FEVIC

This is a project to create a cartridge for the VIC-20 that will breathe some fresh air into this old beauty. Unlike, say, the SD2IEC, the FEVIC will do a lot more than just load games into memory. It _is_ capable of loading any arbitrary code to any location in RAM, but it also contains 64k of SRAM, plus a 64k EEPROM which can be programmed via USB. The ROM, by default, will come with VICMON, but can be written to save any code the user wants. Both the RAM and EEPROM are, of course, paged into 8k sections. The first 16k of RAM goes from $2000-5FFF, and the remaining 48k can be switched in at $6000-7FFF in 8k banks. The EEPROM sits at $A000, which is typically where cartridges sit. On start, the PIC32MX loads the FEVIC menu system into RAM and then sets up the VIC to run it. The user can then show the contents of the EEPROM, load any page into RAM, run VICMON, burn the contents of RAM into the EEPROM, etc.

## Considerations
Anyone using FEVIC to develop must keep in mind that with expanded RAM present, the VIC moves certain things around in memory. The colour RAM is moved to $9400-95FF, screen RAM is moved to $1000-10FF, and BASIC will store its programs starting at $1200. Why it does this, I have absolutely no idea. The expansion memory ranges do not overlap with any of the default locations. 

## Contributing
If you want to contribute to the development of FEVIC, you are more than welcome to! Please just create a fork of the repo, and submit pull requests with sufficient explanation of what the changes do.

### Thanks
This is easily the biggest and most difficult project I have ever tried to develop by myself, and so thanks go out to a number of people who helped keep me sane:

![Screenshot of Welcome Screen](https://i.imgur.com/WJVn16c.png)