EESchema Schematic File Version 4
LIBS:vic20cart-cache
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "VIC20 FrozenCart"
Date ""
Rev ""
Comp "Frozen Electronics"
Comment1 "VIC20 cartridge to rule them all"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L maxlib:VIC20_cart_edge J?
U 1 1 5DE6796F
P 2350 2400
F 0 "J?" H 2350 3625 50  0000 C CNN
F 1 "VIC20_cart_edge" H 2350 3534 50  0000 C CNN
F 2 "FrozenFootprints:VIC20_cartridge" H 2250 1000 50  0001 C CNN
F 3 "~" H 2300 2400 50  0001 C CNN
	1    2350 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DE681F9
P 2000 3600
F 0 "#PWR?" H 2000 3350 50  0001 C CNN
F 1 "GND" H 2005 3427 50  0000 C CNN
F 2 "" H 2000 3600 50  0001 C CNN
F 3 "" H 2000 3600 50  0001 C CNN
	1    2000 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DE6823E
P 2700 3600
F 0 "#PWR?" H 2700 3350 50  0001 C CNN
F 1 "GND" H 2705 3427 50  0000 C CNN
F 2 "" H 2700 3600 50  0001 C CNN
F 3 "" H 2700 3600 50  0001 C CNN
	1    2700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3500 2000 3500
Wire Wire Line
	2000 3500 2000 3600
Wire Wire Line
	2600 3500 2700 3500
Wire Wire Line
	2700 3500 2700 3600
$Comp
L power:+5V #PWR?
U 1 1 5DE68D6E
P 1350 3200
F 0 "#PWR?" H 1350 3050 50  0001 C CNN
F 1 "+5V" H 1365 3373 50  0000 C CNN
F 2 "" H 1350 3200 50  0001 C CNN
F 3 "" H 1350 3200 50  0001 C CNN
	1    1350 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3200 1350 3400
Wire Wire Line
	1350 3400 2100 3400
Text GLabel 2700 1500 2    50   Input ~ 0
A0
Text GLabel 2700 1600 2    50   Input ~ 0
A1
Text GLabel 2700 1700 2    50   Input ~ 0
A2
Text GLabel 2700 1800 2    50   Input ~ 0
A3
Text GLabel 2700 1900 2    50   Input ~ 0
A4
Text GLabel 2700 2000 2    50   Input ~ 0
A5
Text GLabel 2700 2100 2    50   Input ~ 0
A6
Text GLabel 2700 2200 2    50   Input ~ 0
A7
Text GLabel 2700 2300 2    50   Input ~ 0
A8
Text GLabel 2700 2400 2    50   Input ~ 0
A9
Text GLabel 2700 2500 2    50   Input ~ 0
A10
Text GLabel 2700 2600 2    50   Input ~ 0
A11
Text GLabel 2700 2700 2    50   Input ~ 0
A12
Text GLabel 2700 2800 2    50   Input ~ 0
A13
Text GLabel 2700 3300 2    50   Input ~ 0
~RESET
Text GLabel 1950 1500 0    50   Input ~ 0
D0
Text GLabel 1950 1600 0    50   Input ~ 0
D1
Text GLabel 1950 1700 0    50   Input ~ 0
D2
Text GLabel 1950 1800 0    50   Input ~ 0
D3
Text GLabel 1950 1900 0    50   Input ~ 0
D4
Text GLabel 1950 2000 0    50   Input ~ 0
D5
Text GLabel 1950 2100 0    50   Input ~ 0
D6
Text GLabel 1950 2200 0    50   Input ~ 0
D7
Text GLabel 1700 2300 0    50   Input ~ 0
~BLK1
Text GLabel 1950 2400 0    50   Input ~ 0
~BLK2
Text GLabel 1700 2500 0    50   Input ~ 0
~BLK3
Text GLabel 1950 2600 0    50   Input ~ 0
~BLK5
Text GLabel 1700 2700 0    50   Input ~ 0
~RAM1
Text GLabel 1950 2800 0    50   Input ~ 0
~RAM2
Text GLabel 1700 2900 0    50   Input ~ 0
~RAM3
$Comp
L power:GND #PWR?
U 1 1 5DE73984
P 3000 1400
F 0 "#PWR?" H 3000 1150 50  0001 C CNN
F 1 "GND" H 3005 1227 50  0000 C CNN
F 2 "" H 3000 1400 50  0001 C CNN
F 3 "" H 3000 1400 50  0001 C CNN
	1    3000 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DE7435E
P 1650 1400
F 0 "#PWR?" H 1650 1150 50  0001 C CNN
F 1 "GND" H 1655 1227 50  0000 C CNN
F 2 "" H 1650 1400 50  0001 C CNN
F 3 "" H 1650 1400 50  0001 C CNN
	1    1650 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1400 2100 1400
Wire Wire Line
	3000 1400 2600 1400
Wire Wire Line
	2600 1500 2700 1500
Wire Wire Line
	2600 1600 2700 1600
Wire Wire Line
	2600 1700 2700 1700
Wire Wire Line
	2600 1800 2700 1800
Wire Wire Line
	2600 1900 2700 1900
Wire Wire Line
	2600 2000 2700 2000
Wire Wire Line
	2600 2100 2700 2100
Wire Wire Line
	2600 2200 2700 2200
Wire Wire Line
	2600 2300 2700 2300
Wire Wire Line
	2600 2400 2700 2400
Wire Wire Line
	2600 2500 2700 2500
Wire Wire Line
	2600 2600 2700 2600
Wire Wire Line
	2600 2700 2700 2700
Wire Wire Line
	2600 2800 2700 2800
Wire Wire Line
	2100 1500 1950 1500
Wire Wire Line
	2100 1600 1950 1600
Wire Wire Line
	2100 1700 1950 1700
Wire Wire Line
	2100 1800 1950 1800
Wire Wire Line
	2100 1900 1950 1900
Wire Wire Line
	2100 2000 1950 2000
Wire Wire Line
	2100 2100 1950 2100
Wire Wire Line
	2100 2200 1950 2200
Wire Wire Line
	1700 2300 2100 2300
Wire Wire Line
	1950 2400 2100 2400
Wire Wire Line
	1700 2500 2100 2500
Wire Wire Line
	1950 2600 2100 2600
Wire Wire Line
	1700 2700 2100 2700
Wire Wire Line
	1950 2800 2100 2800
Wire Wire Line
	1700 2900 2100 2900
Wire Wire Line
	2700 3300 2600 3300
$Comp
L Memory_Flash:W25Q32JVSS U?
U 1 1 5DE8111E
P 7500 4150
F 0 "U?" H 7500 4731 50  0000 C CNN
F 1 "W25Q32JVSS" H 7500 4640 50  0000 C CNN
F 2 "Package_SO:SOIC-8_5.23x5.23mm_P1.27mm" H 7500 4150 50  0001 C CNN
F 3 "http://www.winbond.com/resource-files/w25q32jv%20revg%2003272018%20plus.pdf" H 7500 4150 50  0001 C CNN
	1    7500 4150
	1    0    0    -1  
$EndComp
$Comp
L maxlib:PIC32MX2XXFXXD U?
U 1 1 5DE835C5
P 12250 2650
F 0 "U?" H 14700 1300 60  0000 C CNN
F 1 "PIC32MX2XXFXXD" H 14350 1400 60  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 12950 2000 60  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX1XX2XX-28-36-44-PIN-DS60001168K.pdf" H 12950 2000 60  0001 C CNN
	1    12250 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B J?
U 1 1 5DE878C3
P 15700 2900
F 0 "J?" H 15470 2889 50  0000 R CNN
F 1 "USB_B" H 15470 2798 50  0000 R CNN
F 2 "" H 15850 2850 50  0001 C CNN
F 3 " ~" H 15850 2850 50  0001 C CNN
	1    15700 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	15400 2900 14950 2900
Wire Wire Line
	15400 3000 14950 3000
$Comp
L power:GND #PWR?
U 1 1 5DE92A3B
P 15750 3500
F 0 "#PWR?" H 15750 3250 50  0001 C CNN
F 1 "GND" H 15755 3327 50  0000 C CNN
F 2 "" H 15750 3500 50  0001 C CNN
F 3 "" H 15750 3500 50  0001 C CNN
	1    15750 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	15700 3300 15700 3500
Wire Wire Line
	15700 3500 15750 3500
Wire Wire Line
	15800 3300 15800 3500
Wire Wire Line
	15800 3500 15750 3500
Connection ~ 15750 3500
$Comp
L power:GND #PWR?
U 1 1 5DE95E41
P 12100 4650
F 0 "#PWR?" H 12100 4400 50  0001 C CNN
F 1 "GND" H 12105 4477 50  0000 C CNN
F 2 "" H 12100 4650 50  0001 C CNN
F 3 "" H 12100 4650 50  0001 C CNN
	1    12100 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	11900 4050 11900 4600
Wire Wire Line
	11900 4600 12000 4600
Wire Wire Line
	12100 4600 12100 4650
Wire Wire Line
	12000 4050 12000 4600
Connection ~ 12000 4600
Wire Wire Line
	12000 4600 12100 4600
Wire Wire Line
	12200 4050 12200 4600
Wire Wire Line
	12200 4600 12100 4600
Connection ~ 12100 4600
Wire Wire Line
	12300 4050 12300 4600
Wire Wire Line
	12300 4600 12200 4600
Connection ~ 12200 4600
$Comp
L Device:CP1_Small C?
U 1 1 5DE9AD5A
P 12100 4300
F 0 "C?" H 12191 4346 50  0000 L CNN
F 1 "1uF" H 12191 4255 50  0000 L CNN
F 2 "" H 12100 4300 50  0001 C CNN
F 3 "~" H 12100 4300 50  0001 C CNN
	1    12100 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	12100 4200 12100 4050
Wire Wire Line
	12100 4400 12100 4600
$Comp
L power:GND #PWR?
U 1 1 5DE9DF6C
P 7100 9550
F 0 "#PWR?" H 7100 9300 50  0001 C CNN
F 1 "GND" H 7105 9377 50  0000 C CNN
F 2 "" H 7100 9550 50  0001 C CNN
F 3 "" H 7100 9550 50  0001 C CNN
	1    7100 9550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DE9E3EC
P 7100 7450
F 0 "#PWR?" H 7100 7300 50  0001 C CNN
F 1 "+5V" H 7115 7623 50  0000 C CNN
F 2 "" H 7100 7450 50  0001 C CNN
F 3 "" H 7100 7450 50  0001 C CNN
	1    7100 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 9500 7100 9550
Wire Wire Line
	7100 7450 7100 7500
Wire Wire Line
	2700 2900 2600 2900
Text GLabel 2900 3000 2    50   Input ~ 0
~IO3
Text GLabel 2700 2900 2    50   Input ~ 0
~IO2
Wire Wire Line
	2600 3000 2900 3000
$Comp
L Memory_EEPROM:28C256 U?
U 1 1 5DF57CF8
P 6400 2000
F 0 "U?" H 6400 3281 50  0000 C CNN
F 1 "28C256" H 6400 3190 50  0000 C CNN
F 2 "" H 6400 2000 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0006.pdf" H 6400 2000 50  0001 C CNN
	1    6400 2000
	1    0    0    -1  
$EndComp
Text GLabel 5750 2900 0    50   Input ~ 0
~BLK5
Text GLabel 1950 3000 0    50   Input ~ 0
V_R~W
Text GLabel 1700 3100 0    50   Input ~ 0
C_R~W
Wire Wire Line
	1950 3000 2100 3000
Wire Wire Line
	2100 3100 1700 3100
Text GLabel 1950 3200 0    50   Input ~ 0
~IRQ
$Comp
L power:+5V #PWR?
U 1 1 5DF88938
P 12150 8350
F 0 "#PWR?" H 12150 8200 50  0001 C CNN
F 1 "+5V" H 12165 8523 50  0000 C CNN
F 2 "" H 12150 8350 50  0001 C CNN
F 3 "" H 12150 8350 50  0001 C CNN
	1    12150 8350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5DF8C9AC
P 13050 8350
F 0 "#PWR?" H 13050 8200 50  0001 C CNN
F 1 "+3.3V" H 13065 8523 50  0000 C CNN
F 2 "" H 13050 8350 50  0001 C CNN
F 3 "" H 13050 8350 50  0001 C CNN
	1    13050 8350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DF8F49E
P 12600 8850
F 0 "#PWR?" H 12600 8600 50  0001 C CNN
F 1 "GND" H 12605 8677 50  0000 C CNN
F 2 "" H 12600 8850 50  0001 C CNN
F 3 "" H 12600 8850 50  0001 C CNN
	1    12600 8850
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AP1117-33 U?
U 1 1 5DF932CF
P 12600 8450
F 0 "U?" H 12600 8692 50  0000 C CNN
F 1 "AP1117-33" H 12600 8601 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 12600 8650 50  0001 C CNN
F 3 "http://www.diodes.com/datasheets/AP1117.pdf" H 12700 8200 50  0001 C CNN
	1    12600 8450
	1    0    0    -1  
$EndComp
Wire Wire Line
	12150 8350 12150 8450
Wire Wire Line
	12150 8450 12300 8450
Wire Wire Line
	12900 8450 13050 8450
Wire Wire Line
	13050 8450 13050 8350
Wire Wire Line
	12600 8750 12600 8850
$Comp
L Memory_RAM:628128_TSOP32 U?
U 1 1 5DE85AA8
P 7100 8500
F 0 "U?" H 7450 9550 50  0000 C CNN
F 1 "628128_TSOP32" H 7450 9450 50  0000 C CNN
F 2 "" H 7100 8500 50  0001 C CNN
F 3 "http://www.futurlec.com/Datasheet/Memory/628128.pdf" H 7100 8500 50  0001 C CNN
	1    7100 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 3200 2100 3200
Text GLabel 6200 9100 0    50   Input ~ 0
~BLK1
Text GLabel 6450 9200 0    50   Input ~ 0
~BLK2
Text GLabel 6200 9300 0    50   Input ~ 0
~BLK3
Wire Wire Line
	6600 9100 6200 9100
Wire Wire Line
	6600 9200 6450 9200
Wire Wire Line
	6600 9300 6200 9300
$Comp
L Diode:1N4148W D?
U 1 1 5DFDAA4D
P 8300 8600
F 0 "D?" H 8300 8383 50  0000 C CNN
F 1 "1N4148W" H 8300 8474 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 8300 8425 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85748/1n4148w.pdf" H 8300 8600 50  0001 C CNN
	1    8300 8600
	-1   0    0    1   
$EndComp
$Comp
L Diode:1N4148W D?
U 1 1 5DFDAE4B
P 8300 8750
F 0 "D?" H 8300 8533 50  0000 C CNN
F 1 "1N4148W" H 8300 8624 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 8300 8575 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85748/1n4148w.pdf" H 8300 8750 50  0001 C CNN
	1    8300 8750
	-1   0    0    1   
$EndComp
$Comp
L Diode:1N4148W D?
U 1 1 5DFDB5F2
P 8300 8900
F 0 "D?" H 8300 8683 50  0000 C CNN
F 1 "1N4148W" H 8300 8774 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 8300 8725 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85748/1n4148w.pdf" H 8300 8900 50  0001 C CNN
	1    8300 8900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7600 8600 8000 8600
Wire Wire Line
	8150 8750 8000 8750
Wire Wire Line
	8000 8750 8000 8600
Connection ~ 8000 8600
Wire Wire Line
	8000 8600 8150 8600
Wire Wire Line
	8150 8900 8000 8900
Wire Wire Line
	8000 8900 8000 8800
Connection ~ 8000 8750
Text GLabel 8750 8600 2    50   Input ~ 0
~BLK1
Text GLabel 8750 8750 2    50   Input ~ 0
~BLK2
Text GLabel 8750 8900 2    50   Input ~ 0
~BLK3
Wire Wire Line
	8750 8600 8450 8600
Wire Wire Line
	8750 8750 8450 8750
Wire Wire Line
	8750 8900 8450 8900
$Comp
L Device:R R?
U 1 1 5DFE8A0F
P 8300 8400
F 0 "R?" V 8093 8400 50  0000 C CNN
F 1 "1k" V 8184 8400 50  0000 C CNN
F 2 "" V 8230 8400 50  0001 C CNN
F 3 "~" H 8300 8400 50  0001 C CNN
	1    8300 8400
	0    1    1    0   
$EndComp
Wire Wire Line
	8150 8400 8000 8400
Wire Wire Line
	8000 8400 8000 8600
$Comp
L power:+5V #PWR?
U 1 1 5DFEB7AE
P 8600 8250
F 0 "#PWR?" H 8600 8100 50  0001 C CNN
F 1 "+5V" H 8615 8423 50  0000 C CNN
F 2 "" H 8600 8250 50  0001 C CNN
F 3 "" H 8600 8250 50  0001 C CNN
	1    8600 8250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 8400 8450 8400
$Comp
L 74xx:74AHC04 U?
U 7 1 5DFF49D5
P 14950 8750
F 0 "U?" H 15180 8796 50  0000 L CNN
F 1 "74AHC04" H 15180 8705 50  0000 L CNN
F 2 "" H 14950 8750 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT04.pdf" H 14950 8750 50  0001 C CNN
	7    14950 8750
	1    0    0    -1  
$EndComp
Text GLabel 7700 8900 2    50   Input ~ 0
V_R~W
Wire Wire Line
	7700 8900 7600 8900
Wire Wire Line
	7600 8800 8000 8800
Connection ~ 8000 8800
Wire Wire Line
	8000 8800 8000 8750
Text GLabel 6450 7700 0    50   Input ~ 0
A0
Text GLabel 6450 7800 0    50   Input ~ 0
A1
Text GLabel 6450 7900 0    50   Input ~ 0
A2
Text GLabel 6450 8000 0    50   Input ~ 0
A3
Text GLabel 6450 8100 0    50   Input ~ 0
A4
Text GLabel 6450 8200 0    50   Input ~ 0
A5
Text GLabel 6450 8300 0    50   Input ~ 0
A6
Text GLabel 6450 8500 0    50   Input ~ 0
A8
Text GLabel 6450 8400 0    50   Input ~ 0
A7
Text GLabel 6450 8600 0    50   Input ~ 0
A9
Text GLabel 6450 8700 0    50   Input ~ 0
A10
Text GLabel 6450 8800 0    50   Input ~ 0
A11
Text GLabel 6450 8900 0    50   Input ~ 0
A12
Text GLabel 6450 9000 0    50   Input ~ 0
A13
Wire Wire Line
	6450 7700 6600 7700
Wire Wire Line
	6450 7800 6600 7800
Wire Wire Line
	6450 7900 6600 7900
Wire Wire Line
	6450 8000 6600 8000
Wire Wire Line
	6450 8100 6600 8100
Wire Wire Line
	6450 8200 6600 8200
Wire Wire Line
	6450 8300 6600 8300
Wire Wire Line
	6450 8400 6600 8400
Wire Wire Line
	6450 8500 6600 8500
Wire Wire Line
	6450 8600 6600 8600
Wire Wire Line
	6450 8700 6600 8700
Wire Wire Line
	6450 8800 6600 8800
Wire Wire Line
	6450 8900 6600 8900
Wire Wire Line
	6450 9000 6600 9000
Wire Wire Line
	8600 8250 8600 8300
Text GLabel 7750 7700 2    50   Input ~ 0
D0
Text GLabel 7750 7800 2    50   Input ~ 0
D1
Text GLabel 7750 7900 2    50   Input ~ 0
D2
Text GLabel 7750 8000 2    50   Input ~ 0
D3
Text GLabel 7750 8100 2    50   Input ~ 0
D4
Text GLabel 7750 8200 2    50   Input ~ 0
D5
Text GLabel 7750 8300 2    50   Input ~ 0
D6
Text GLabel 7750 8400 2    50   Input ~ 0
D7
Wire Wire Line
	7750 8400 7600 8400
Wire Wire Line
	7600 8300 7750 8300
Wire Wire Line
	7600 8200 7750 8200
Wire Wire Line
	7600 8100 7750 8100
Wire Wire Line
	7600 8000 7750 8000
Wire Wire Line
	7600 7900 7750 7900
Wire Wire Line
	7600 7800 7750 7800
Wire Wire Line
	7600 7700 7750 7700
Wire Wire Line
	7950 8700 7950 8300
Wire Wire Line
	7950 8300 8600 8300
Wire Wire Line
	7600 8700 7950 8700
Connection ~ 8600 8300
Wire Wire Line
	8600 8300 8600 8400
Wire Wire Line
	6000 2900 5900 2900
Wire Wire Line
	6000 2800 5900 2800
Wire Wire Line
	5900 2800 5900 2900
Connection ~ 5900 2900
Wire Wire Line
	5900 2900 5750 2900
Text GLabel 5850 1100 0    50   Input ~ 0
A0
Text GLabel 5850 1200 0    50   Input ~ 0
A1
Text GLabel 5850 1300 0    50   Input ~ 0
A2
Text GLabel 5850 1400 0    50   Input ~ 0
A3
Text GLabel 5850 1500 0    50   Input ~ 0
A4
Text GLabel 5850 1600 0    50   Input ~ 0
A5
Text GLabel 5850 1700 0    50   Input ~ 0
A6
Text GLabel 5850 1900 0    50   Input ~ 0
A8
Text GLabel 5850 1800 0    50   Input ~ 0
A7
Text GLabel 5850 2000 0    50   Input ~ 0
A9
Text GLabel 5850 2100 0    50   Input ~ 0
A10
Text GLabel 5850 2200 0    50   Input ~ 0
A11
Text GLabel 5850 2300 0    50   Input ~ 0
A12
Text GLabel 5850 2400 0    50   Input ~ 0
A13
Wire Wire Line
	5850 1100 6000 1100
Wire Wire Line
	5850 1200 6000 1200
Wire Wire Line
	5850 1300 6000 1300
Wire Wire Line
	5850 1400 6000 1400
Wire Wire Line
	5850 1500 6000 1500
Wire Wire Line
	5850 1600 6000 1600
Wire Wire Line
	5850 1700 6000 1700
Wire Wire Line
	5850 1800 6000 1800
Wire Wire Line
	5850 1900 6000 1900
Wire Wire Line
	5850 2000 6000 2000
Wire Wire Line
	5850 2100 6000 2100
Wire Wire Line
	5850 2200 6000 2200
Wire Wire Line
	5850 2300 6000 2300
Wire Wire Line
	5850 2400 6000 2400
Text GLabel 6950 1100 2    50   Input ~ 0
D0
Text GLabel 6950 1200 2    50   Input ~ 0
D1
Text GLabel 6950 1300 2    50   Input ~ 0
D2
Text GLabel 6950 1400 2    50   Input ~ 0
D3
Text GLabel 6950 1500 2    50   Input ~ 0
D4
Text GLabel 6950 1600 2    50   Input ~ 0
D5
Text GLabel 6950 1700 2    50   Input ~ 0
D6
Text GLabel 6950 1800 2    50   Input ~ 0
D7
Wire Wire Line
	6950 1800 6800 1800
Wire Wire Line
	6800 1700 6950 1700
Wire Wire Line
	6800 1600 6950 1600
Wire Wire Line
	6800 1500 6950 1500
Wire Wire Line
	6800 1400 6950 1400
Wire Wire Line
	6800 1300 6950 1300
Wire Wire Line
	6800 1200 6950 1200
Wire Wire Line
	6800 1100 6950 1100
Text GLabel 5850 2500 0    50   Input ~ 0
EEPAGE
Wire Wire Line
	6000 2500 5850 2500
Text GLabel 9400 1600 0    50   Input ~ 0
D0
Text GLabel 9400 1700 0    50   Input ~ 0
D1
Text GLabel 9400 1800 0    50   Input ~ 0
D2
Text GLabel 9400 2000 0    50   Input ~ 0
D4
Text GLabel 9400 2100 0    50   Input ~ 0
D5
Text GLabel 9400 2200 0    50   Input ~ 0
D6
Text GLabel 9400 2300 0    50   Input ~ 0
D7
Wire Wire Line
	9400 2300 9550 2300
Wire Wire Line
	9550 2200 9400 2200
Wire Wire Line
	9550 2100 9400 2100
Wire Wire Line
	9550 2000 9400 2000
Wire Wire Line
	9550 1800 9400 1800
Wire Wire Line
	9550 1700 9400 1700
Wire Wire Line
	9550 1600 9400 1600
Wire Wire Line
	9550 1900 9400 1900
Text GLabel 9400 1900 0    50   Input ~ 0
D3
Text GLabel 15250 2000 2    50   Input ~ 0
EEPAGE
Wire Wire Line
	15250 2000 14950 2000
Text GLabel 15100 2600 2    50   Input ~ 0
~IO3
Wire Wire Line
	15100 2600 14950 2600
$EndSCHEMATC
