	;; Currently the origin of this code is at $A000, 40960 decimal, to emulate
	;; where it will be when it's on cartridge
	;; Changing the origin is done by changing the Makefile

	;; KERNAL jump table
	chrout 	= 	$FFD2	; output character to channel, by default screen (device 3)
	getin 	= 	$FFE4	; get a character from keyboard buffer
	plot	=	$FFF0	; with carry set, return cursor in X/Y, with carry clear, set cursor to X/Y
	load	=	$FFD5	; load RAM from a device (!)
	chkin	=	$FFC6	; open channel for input
	chkout	=	$FFC9	; open channel for output
	chrin	=	$FFCF	; input character from channel, by default keyboard (device 1)
	softreset =	$FD22	; soft reset back to basic

	;; sound registers
	lowosc	=	$900A
	midosc	=	$900B
	highosc	=	$900C
	noiseosc =	$900D
	colorram =	$9400 		; this is the non-standard location
	;; colorram = 	$9600		; this is the standard location
	via1	=	$9110
	via2	=	$9120

	jmp start

	;; prtstr parameters
	;; string to print pointed to by $30/$31
	;; saves Y
prtstr:	tya
	pha			; put Y on the stack in case calling function is using it
	ldy #$00		; clear the Y register
prtlp:	lda ($30),Y		; index the message with Y
	beq xprtstr		; once we hit a 0, end
	jsr chrout
	iny			; move to next character
	;; I fucking SWEAR the VIC was not spreading my strings over multiple lines, and this
	;; code was necessary. But suddenly it has started doing that, and so this had to be
	;; commented out to prevent double-spacing of lines. What the fuck?!
	;; tya			; we need to check Y mod 22
	;; sta $40
	;; tay			; move a back to y?
	;; lda #$16
	;; sta $41
	;; jsr modu
	;; cmp #00			; are we at the end of the line?
	;; bne prtlp		; if not, keep going
	jmp prtlp
	lda #$0D
	jsr chrout		; otherwise, add a return
	jmp prtlp
xprtstr:
	pla			; grab the y from the stack
	tay			; restore y register
	rts

	;; getstr parameters
	;; pointer to string storage will be at $30/$31
	;; returns length of string in $32
	;; saves Y register
getstr:	tya
	pha			; save Y
	ldy #$00		; clear Y
getstrlp:
	jsr getin
	cmp #$0D
	beq getstrdone
	sta ($30),Y
	iny
	jmp getstrlp
getstrdone:
	sty $32			; length of string at $32
	pla
	tay			; restore Y
	rts

	;; dechex
	;; this will only work with 4-digit hex numbers!
	;; returns the computed value in $35/36
	;; the string must be pointed to by $30/31, length in $32
dechex:	lda $32
	cmp #3
	beq dechex1
	lda #$04
	jmp err
dechex1:
	ldy #$0
	lda ($30),Y
	;; decode it
	iny
	cmp #$4
	bne dechex1
	rts
	

	;; err parameters
	;; a will contain the error code
	;; we just print the error and then go to the main menu
err:	sta $29
	lda #<serror
	sta $30
	lda #>serror
	sta $31
	jsr prtstr
	lda $29
	clc
	adc #$30
	jsr chrout
	lda #$0D
	jsr chrout
	jsr _wait
	jmp start

	;; prhex parameters
	;; a must contain a number to be printed in hex
prhex:	pha
	lsr
	lsr
	lsr
	lsr
	jsr prnybble
	pla
	and #15
prnybble:
	sed
	clc
	adc #$90
	adc #$40
	cld
	jsr chrout
	rts
	;; modu parameters
	;; this routine does $40 mod $41
	;; returns the modulo in A
modu:	lda $40
	sec
modu1:	sbc $41
	bcs modu1
	adc $41
	rts

	;; _wait parameters
	;; this prints the "PRESS RETURN" message
	;; waits for the return key, and then returns
_wait:	lda #<spress
	sta $30
	lda #>spress
	sta $31
	jsr prtstr
_wait2:	lda #$00
	jsr getin
	cmp #$0D		; wait for enter key
	bne _wait2
	rts

	;; ====== main code ======

menuchoice:
	

mainmenu:
	lda #$00
	jsr getin
	cmp #$31
	beq one
	cmp #$32
	beq two
	cmp #$33
	beq three
	cmp #$34
	beq four
	cmp #$35
	beq five
	cmp #$0D
	bne mainmenu

	;; they pressed 1
one:	jsr loadmenu

	;; they pressed 2
two:	lda #02
	jmp err
	;; they pressed 3
three:	jmp $6002		; go to VICMON
	brk
four:
five:	lda #<sexiting
	sta $30
	lda #>sexiting
	sta $31
	jsr prtstr
	jsr _wait
	jmp softreset
	
loadmenu:
	lda #$93
	jsr chrout
	lda #<sloadmenu
	sta $30
	lda #>sloadmenu
	sta $31
	jsr prtstr
	jsr _wait

start:	lda #$93
	jsr chrout
	;; set up border & background color
	lda #$0A
	sta $900F
	;; move cursor to 2,3 (x and y are backwards for some reason)
	ldx #$03
	ldy #$02
	clc
	jsr plot
	;; print welcome message
	ldy #$00
	lda #<swelcome
	sta $30
	lda #>swelcome
	sta $31
	jsr prtstr
	;; print main menu
	lda #<smainmenu
	sta $30
	lda #>smainmenu
	sta $31
	jsr prtstr
	jsr mainmenu
	brk

	;; ====== strings ======
swelcome:	.asc $1c,"welcome to fevic!",$1f,$0D,0
spress:		.asc $1f,"press return.",$0D,0
stest:		.asc "this is a really long string that will have to be divided across multiple lines",0
smainmenu:	.asc "1) load from flash",$0D,"2) run loaded program",$0D,"3) vicmon",$0D,"4) burn eeprom",$0D,"5) return to basic",$0D,0
sloadmenu:	.asc "1) show list",$0D,"2) choose program",$0D,0
sloading:	.asc "loading...",$0D,0
srunning:	.asc "running...",$0D,0
sexiting:	.asc "exiting...",$0D,0
smemquery:	.asc "address to load at: ",$0D,0
srunquery:	.asc "address to run from: ",$0D,0
serror:		.asc "the error was: ",0
